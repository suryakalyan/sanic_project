from sanic import Sanic

from main_working_dir.End_Points.operations import ConfigureRotes

app = Sanic('operations')
ConfigureRotes(app)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, workers=2, debug=True)
