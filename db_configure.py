from pymongo import MongoClient

#client = MongoClient('10.32.102.171:27017')
client = MongoClient('10.32.102.171:27017',minPoolSize=1,maxPoolSize=200,waitQueueTimeoutMS=1,waitQueueMultiple=1)
print(client)
db = client.Sanic_Operations
connections_dict = db.command("serverStatus")["connections"]
print("Server Status:   ",connections_dict)
print(db.current_op())

#print(client.runCommand( { "connPoolStats" : 1 }))