from xpms_storage.db_handler import DBProvider


def db_insert(table, request_data):
    try:
        if request_data == '':
            raise Exception("please provide required data")
        db = DBProvider.get_instance(db_name='Sanic_Operations')
        db.insert(table="{0}_{1}".format("document",table), rows=request_data, dup_update=True)

    except Exception as e:
        print("Exception ", str(e))
        raise e
