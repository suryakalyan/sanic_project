import os

import db_configure
from db_utils.db_handler import db_insert
client = db_configure.client
db_handler = db_configure.db

class EmpServices:
    def get(self):
        return Emp_Details()

class Emp_Details:
    def emp_infrm(self, empinformation):
        connections_dict = db_handler.command("serverStatus")["connections"]
        print("Server Status:   ", connections_dict)
        db_handler.Sanic_Operations.insert_one({
            "name": empinformation['name'],
            "role": empinformation['role'],
            "Team": empinformation['Team'],
            "Manager":empinformation['Manager']
        })
        client.close()
        return empinformation

