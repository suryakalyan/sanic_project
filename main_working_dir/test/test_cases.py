import json
import unittest
from sanic import Sanic
from line_profiler import LineProfiler
from main_working_dir.End_Points.operations import ConfigureRotes

@profile
class MyTestCase(unittest.TestCase):


    def test_developer_information(self):
        app = Sanic('operations')
        print(app)
        ConfigureRotes(app)
        url = "http://0.0.0.0:8000/get/devloper/information"
        requestt,response = app.test_client.get(url)
        assert response.status == 200

    def test_saving_dev_information(self):
        app = Sanic('operations')
        print(app)
        ConfigureRotes(app)
        data ={
                "name": "kHarsha",
                "role": "UI Developer",
                "Team": "Pulse",
                "Manager": "Satish Schalla"
            }
        headers = {
            'Content-Type': "application/json",
            'Accept': "application/json"
            }
        url = "http://0.0.0.0:8000/devloper/informarion"
        requestt, response = app.test_client.post(url,data=json.dumps(data),headers= headers)
        assert response.status == 200

o=MyTestCase()
profile = LineProfiler(primes(100))
profile.print_stats()