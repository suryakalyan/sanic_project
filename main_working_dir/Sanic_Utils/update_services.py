import db_configure

db_handler = db_configure.db

class Update_Information:
    def infrm_update(self,**kwargs):

        connections_dict = db_handler.command("serverStatus")["connections"]
        print("Server Status:   ", connections_dict)
        emp_res=db_handler.Sanic_Operations.find_one({'name': kwargs['name']})
        if kwargs.get('update name', None):
            emp_res['name'] = kwargs['update name']
        elif kwargs.get("role", None):
            emp_res['role'] = kwargs.get('role')
        elif kwargs.get('Team',None) :
            emp_res['Team'] = kwargs['Team']
        elif kwargs.get('Manager',None) != '':
            emp_res['Manager'] = kwargs["Manger"]
        db_handler.Sanic_Operations.delete_one({"_id":emp_res['_id']})
        db_handler.Sanic_Operations.insert_one({ "name": emp_res['name'],
                                                "role": emp_res['role'],
                                                "Team": emp_res['Team'],
                                                "Manager":emp_res['Manager']
                                                })
        emp_res.pop('_id')
        db_configure.client.close()
        return emp_res



