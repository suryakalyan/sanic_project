from sanic import response
from sanic.config import Config
from main_working_dir.Sanic_Utils.emp_serices import EmpServices
from main_working_dir.Sanic_Utils.get_emp_details import Employee_Details
from main_working_dir.Sanic_Utils.update_services import Update_Information

# app = Sanic('operations')
# print(app)
# console_bp = Blueprint('test_bp')


Config.KEEP_ALIVE = False

def ConfigureRotes(app):

    @app.route("/")
    def test(request):
        return response.text("Hello World!")

    @app.route("/devloper/informarion", methods=["POST"])
    async def devpoer_information(request):
        try:
            print(request.json)
            emp_ser = EmpServices().get()
            emp_response=emp_ser.emp_infrm(request.json)
            return response.json({"Status": "Sucess","Empolyee Information": emp_response})
        except Exception as e:
            print(str(e))
            return response.json({"Status": "Falied", "Empolyee Information": emp_response})
    @app.route("update/devloper/information", methods=['PUT'])
    async def update_developer_information(request):
        try:
            print(request.json)
            res=Update_Information().infrm_update(**request.json)
            return response.json({"Status": "Upated Successfully", "Empolyee Information": res})

        except Exception as e:
            print(str(e))

    @app.route("get/devloper/information", methods=['GET'])
    async def get_developer_information(request):
        try:
            print(request.json)
            res=Employee_Details().emp_details()
            return response.json({"Status": "Success","Empolyee Information": res})

        except Exception as e:
            print(str(e))

