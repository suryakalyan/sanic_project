import threading

from pymongo import MongoClient


def worker(num, db):
    print("Thread number %s" % num)
    try:
        connections_dict = database.command("serverStatus")["connections"]
        #print("Request Number:  ",connections_dict)
        print("Current Request is {0}, {1}".format(num,connections_dict))
        print("Processed Request",db.test.insert({"a":num, "b":num*2}))
    except Exception as e:
        print("Exception: %s" % e)
    return

client = MongoClient('10.32.102.171:27017',maxPoolSize=10,waitQueueTimeoutMS=100,waitQueueMultiple=100)

database = client.Sanic_Operations
connections_dict = database.command("serverStatus")["connections"]
print("Starting Server",connections_dict)

threads = []
print(threads)
for i in range(500):
    print("I Values:  ",i)
    t = threading.Thread(target=worker, args=(i, database))
    threads.append(t)
    # Start thread.
    t.start()

# Wait for all threads to complete.
# for t in threads:
#     t.join()